﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace TPT
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new InheritanceMappingContext())
            {
                CreditCard creditCard = new CreditCard()
                {
                    Number = "987654321",
                    CardType = 1
                };
                User user = new User()
                {
                    UserId = 1,
                    BillingInfo = creditCard
                };
                context.Users.Add(user);
                context.SaveChanges();
            }
        }
    }
    public abstract class BillingDetail
    {
        public int BillingDetailId { get; set; }
        public string Owner { get; set; }
        public string Number { get; set; }
    }

    [Table("BankAccounts")]
    public class BankAccount : BillingDetail
    {
        public string BankName { get; set; }
        public string Swift { get; set; }
    }
    
    [Table("CreditCards")]
    public class CreditCard : BillingDetail
    {
        public int CardType { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class InheritanceMappingContext : DbContext
    {
        public DbSet<BillingDetail> BillingDetails { get; set; }
        public DbSet<User> Users { get; set; }
    }
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int BillingDetailId { get; set; }

        public virtual BillingDetail BillingInfo { get; set; }
    }
}
