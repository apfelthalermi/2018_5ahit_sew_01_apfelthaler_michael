﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace OneToOne
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new SchoolDBContext())
            {
                var stud = new Student() { StudentName = "Will" };

                ctx.Students.Add(stud);
                ctx.SaveChanges();
            }
        }
    }
}
