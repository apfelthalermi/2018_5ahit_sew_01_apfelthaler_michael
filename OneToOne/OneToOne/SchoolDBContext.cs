﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OneToOne
{
    class SchoolDBContext:DbContext
    {
        public SchoolDBContext() : base()
        {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                        .HasRequired(s => s.Address)
                        .WithRequiredPrincipal(ad => ad.Student);

        }
        public DbSet<Student> Students { get; set; }
    }
}
