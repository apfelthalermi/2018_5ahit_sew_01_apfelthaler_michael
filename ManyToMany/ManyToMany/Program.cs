﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new SchoolDBContext())
            {
                var course = new Course() { CourseName = "Will" };

                ctx.Courses.Add(course);
                ctx.SaveChanges();
            }
        }
    }
}
