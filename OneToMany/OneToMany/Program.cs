﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new SchoolContext())
            {
                var course = new Grade() { GradeName = "Will" };

                ctx.Grades.Add(course);
                ctx.SaveChanges();
            }
        }
    }
}