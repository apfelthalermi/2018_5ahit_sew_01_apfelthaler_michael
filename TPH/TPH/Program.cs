﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace TPH
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new InheritanceMappingContext())
            {
                var stud = new BankAccount() { BankName = "Will" };

                ctx.BillingDetails.Add(stud);
                ctx.SaveChanges();
            }
        }
    }
    public abstract class BillingDetail
    {
        public int BillingDetailId { get; set; }
        public string Owner { get; set; }
        public string Number { get; set; }
    }

    public class BankAccount : BillingDetail
    {
        public string BankName { get; set; }
        public string Swift { get; set; }
    }

    public class CreditCard : BillingDetail
    {
        public int CardType { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class InheritanceMappingContext : DbContext
    {
        public InheritanceMappingContext() : base()
        {

        }

        public DbSet<BillingDetail> BillingDetails { get; set; }
    }
}
